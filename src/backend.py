'''
Created on 3 Jun 2015

@author: Elefhterios Avramidis
'''

import itertools
import os
import re
import subprocess
import logging as log
from stat import ST_CTIME
import sys
import filecmp
import shutil


def analyze_filename(filename):
    """
    Break a single filename into fields describing the dataset, 
    following the naming convention for pilots in QTleap
    @param filename: the filename to be analyzed
    @type filename: string
    @return: pilot id, source language, target language, batch name
    @rtype: tuple of strings
    """
    try:
        pilot, source_language, target_language, batch = re.findall('([^-]*)-([^-]*)-([^-]*)-([^-]*)\.txt', os.path.basename(filename))[0]
        log.info("Found {}".format((pilot, source_language, target_language, batch)))
        return pilot, source_language, target_language, batch
    except TypeError:
        pass
    
def construct_filename(path_repository, pilot, source_language, target_language, batch):
    """
    Get the filename of a file given the dataset characteristics. 
    @param path_repository: the root path of the translations repository    
    """
    return os.path.join(path_repository, pilot, "-".join([pilot, source_language, target_language, batch]) + ".txt")

def construct_filename_original(path_repository, language, batch):
    batch = batch.replace("batch", "Batch") #capitalize first letter
    return os.path.join(path_repository, "references", "_".join([batch, language]) + ".txt")


def analyze_filenames(path):
    
    filenames = []
    print path
    for relative_dir, _, basenames in os.walk(path):
        for basename in basenames:
            filenames.append(os.path.join(path, relative_dir, basename))
    pilots = set()
    language_pairs = set()
    batches = set()
    for filename in filenames:
        try:
            filename_analysis = analyze_filename(filename)
            if not filename_analysis:
                continue
            pilot, source_language, target_language, batch = filename_analysis 
            pilots.add(pilot)
            language_pairs.add((basic_language_name(source_language, target_language), source_language, target_language))
            batches.add(batch)
        except IndexError:
            pass
    
    #sort alphabetically
    pilots = sorted(list(pilots))
    #sort based on language full name
    language_pairs = sorted(list(language_pairs)) 
    #remove language full name from tuples
    language_pairs = [(source_language, target_language) for _, source_language, target_language in language_pairs]
    batches = sorted(list(batches))    
    
    return pilots, language_pairs, batches

def git_metadata(path_repository):
    os.chdir(path_repository)
    gitlog = subprocess.check_output(["git", "log", "--date=iso"])
    #commit_id = gitlog.split('\n')[0].split()[1]
    #commit_link = "<a href='{}/repository/revisions/{}'>{}</a>".format(path_repository, commit_id, commit_id[:8])
    
    #print gitlog
    date, time = re.findall("Date:\s*([^ ]*)\s([^ ]*)", gitlog)[0]
    datestring = "{}_{}".format(date, time)
    #return datestring
    return datestring

def git_pilotversion(path_file):
    path_repository = os.path.dirname(path_file)
    os.chdir(path_repository)    
    gitlog = subprocess.check_output(["git", "log", path_file])
    versions = gitlog.count("Date:")-1
    return versions


def get_previous_experiment(dirpath, pilot):
    #taken from http://stackoverflow.com/questions/168409/how-do-you-get-a-directory-listing-sorted-by-creation-date-in-python
    
    try:
        listdir = os.listdir(dirpath)
    except OSError:
        return None
    # get all entries in the directory w/ stats
    entries = (os.path.join(dirpath, fn) for fn in listdir)
    entries = ((os.stat(path), path) for path in entries)
    
    # leave only regular files, insert creation date
    entries = ((stat[ST_CTIME], path)
               for stat, path in entries if os.path.isdir(path) and pilot in path)
    #NOTE: on Windows `ST_CTIME` is a creation date 
    #  but on Unix it could be something else
    #NOTE: use `ST_MTIME` to sort by a modification date
    
    sorted_entries = sorted(entries)
    if not sorted_entries:
        log.info("Did not find previous dir {}".format(dirpath))
        return None
    last_entry = sorted_entries[-1]
    last_dir = last_entry[1]
    return last_dir
        

def git_pull(path_repository):
    os.chdir(path_repository)
    output = subprocess.check_output(["git", "pull"])
    if "Already up-to-date." in output:
        #log.info("Git already up to date")
        return False
    return True


def copy_originaltext_if_needed(directory_experiment, text_type, path_repository, language, batch):
    #create paths
    filename = os.path.join(directory_experiment, "{}.txt".format(text_type))
    filename_original = construct_filename_original(path_repository, language, batch)
    if not os.path.isfile(filename_original):
        return    
 
    #we have already checked whether the task has translation data by at least one system
    
    #now check whether an original text has been placed in the task root folder, if not, copy it there
    if not os.path.exists(filename):
        log.info("Moving {} to: {}".format(text_type, filename))
        shutil.copy(filename_original, filename)
    #if an original text is already there, then copy only if there is a text change
    elif not filecmp.cmp(filename, filename_original):
        log.info("Moving {} to: {}".format(text_type, filename))
        shutil.copy(filename_original, filename)
    else:
        log.info("No {} update was required for {}".format(text_type, directory_experiment))


def copy_translation_if_needed(directory_experiment, pilot, datestring, commit_id, filename_repository_translation):
    #Deprecated function 
    directory_translation = os.path.join(directory_experiment, "{}_{}".format(pilot, commit_id)) 
    filename_translation = os.path.join(directory_translation, "translation.txt")
    
    directory_previous_experiment = get_previous_experiment(directory_experiment, pilot)
    
    if directory_previous_experiment:
        filename_previous_translation = os.path.join(directory_previous_experiment, "translation.txt")
        log.info("Comparing files {} and {}".format(filename_repository_translation, filename_previous_translation))
        equalcheck = filecmp.cmp(filename_repository_translation, filename_previous_translation)
        log.info("Are files equal? {}".format(equalcheck))
        if equalcheck:
            return False
    
    
    if not os.path.exists(directory_translation):
        os.makedirs(directory_translation)
    versions = git_pilotversion(filename_repository_translation)
    log.debug("Moving translation to {}".format(filename_translation))
    shutil.copy(filename_repository_translation, filename_translation)
    description = get_custom_description(filename_repository_translation)
    
    
    create_system_description(directory_translation, pilot, datestring, commit_id, versions, description)
    return True        

def get_custom_description(filename_repository_translation):
    metafilename = filename_repository_translation.replace(".txt", ".meta")
    try:
        metadescription = open(metafilename, 'r').read()
        metadescription = metadescription.replace('\n', '<br/>')
        for repository_name, revision in re.findall("https://github.com/ufal/([^/]*)/commit/([a-z0-9]*)", metadescription):
            plain_link = "https://github.com/ufal/{repository_name}/commit/{revision}".format(repository_name=repository_name,
                                                                                        revision=revision)
            formatted_link = "{repository_name}: <a href='{link}'>{revision}</a>".format(repository_name=repository_name,
                                                                                         link=plain_link, 
                                                                                         revision=revision[:5])
            metadescription = metadescription.replace(plain_link, formatted_link)
        
        return metadescription
    except:
        return ""

def create_system_description(directory_translation, pilot, datestring, commit_id, versions, meta_description, author, comment, url_repository_revisions):
    
    commit_url = "<a href='{}/{}' title='{}'>{}</a>".format(url_repository_revisions, commit_id, commit_id, commit_id[:5])
    system_name = "Pilot {pilot}.{ver:02d}".format(pilot=pilot[5:], ver=versions)
    description = "Date: {}<br/>Translation commit: {}<br/>Author: {}<br/>Commit comment: {}".format(datestring, commit_url, author, comment)
    
    if meta_description.strip()!="":
        description = "{}<br/>{}".format(description, meta_description.replace("\n", "<br>"))
        
    metadata = 'translation: translation.txt\nname: "{name}"\nprecompute_ngrams: true\ndescription: "{description}"'.format(name=system_name, description=description)
    metadata_filename = os.path.join(directory_translation, "config.neon") 
    f = open(metadata_filename, 'w')
    f.write(metadata)
    f.close()
    
def basic_language_name(source_language, target_language):
    language_name = {"bg": "Bulgarian",
                  "cs": "Czech",
                  "de": "German", 
                  "eu": "Basque",
                  "nl": "Dutch",
                  "pt": "Portoguese",
                  "es": "Spanish"
                  }
    try:
        return language_name[source_language]
    except:
        return language_name[target_language]
    


def create_experiment_description(directory_experiment, source_language, target_language, batch):
    batch_type = {"q": "questions", "a": "answers"}

    basic_language = basic_language_name(source_language, target_language)
    
    if "batch" in batch:
        batch_id = batch[5:-1]
        batch_symbol = batch[-1]
        try:
            batch = batch_type[batch_symbol]
        except:
            #a key was given that does not match the existing batches
            return
        
        system_name = "{}: {}->{} - Batch {} {}".format(basic_language, source_language, target_language, batch_id, batch)
    else:
        system_name = "{}: {}->{} - {}".format(basic_language, source_language, target_language, batch)
    
    description = ""
    metadata = "source: source.txt\nreference: reference.txt\nname: \"{}\"\ndescription: {}".format(system_name, description)
    metadata_filename = os.path.join(directory_experiment, "config.neon") 
    f = open(metadata_filename, 'w')
    f.write(metadata)
    f.close()


def update(path_repository, path_evaluation, url_repository_revisions, debug=False):
    
    #pull latest translations and proceed only if they have changed 
    #or if we want for debugging purpose to re-import them
    if not git_pull(path_repository) and not debug:
        return 
            
    #look in the folders and get a list of pilot names, language pairs and batches that have translations for
    pilots, language_pairs, batches = analyze_filenames(path_repository)

    #iterate over all combinations of language pairs and batches 
    for language_pair, batch in itertools.product(language_pairs, batches):
        source_language, target_language = language_pair
        directory_experiment = os.path.join(path_evaluation, "-".join([source_language, target_language, batch]))
        
        log.info("*****")
        log.info("{}, {}".format(language_pair, batch))

        #iterate over all pilots
        for pilot in pilots:    
            
            #get the filename of the latest translation for the requested pilot, language pair and batch
            filename_repository_translation = construct_filename(path_repository, pilot, source_language, target_language, batch)
            
            #for some pilots not all batch translations are available. Skip those
            if not (os.path.exists(filename_repository_translation)):
                log.info("File {} not found".format(filename_repository_translation))
                continue
            else:
                log.info("File {} found".format(filename_repository_translation))
                #copy the new translation in the interface data folder, only if translation has been modified
                #copy_translation_if_needed(directory_experiment, pilot, datestring, commit_id, filename_repository_translation)
                copy_missing_commits(directory_experiment, pilot, path_repository, filename_repository_translation, url_repository_revisions)
        
        #place source and reference text in experiment folder, only if the experiment has been just created
        #or if the source/reference text has been modified
        if os.path.exists(directory_experiment):
            copy_originaltext_if_needed(directory_experiment, "source", path_repository, source_language, batch)
            copy_originaltext_if_needed(directory_experiment, "reference", path_repository, target_language, batch)
            create_experiment_description(directory_experiment, source_language, target_language, batch)


def copy_missing_commits(directory_experiment, pilot, path_repository, filename_repository_translation, url_repository_revisions):
    
    #get basic info for all commits from the repository
    os.chdir(path_repository)
    gitlog = subprocess.check_output(["git", "log", "--date=iso", filename_repository_translation]) 
    commit_ids = re.findall("commit ([a-z0-9]*)", gitlog)
    log.info("Commit_ids extracted:{}".format(commit_ids))
    datestrings = re.findall("Date:\s*([^ ]*\s[^ ]*)", gitlog)
    log.info("Datestrings extracted:{}".format(datestrings))
    authors = re.findall("Author: ([^<]*)", gitlog)
    log.info("Authors extracted:{}".format(authors))
    comments = [" ".join(re.findall("\s{4}(.*)", commit)).strip() for commit in gitlog.split("commit")[1:]]
    log.info("comments extracted:{}".format(comments))

    #version counter
    version = 0
    
    #iterate over all the versions of this particular translation file from the git history
    for commit_id, datestring, author, comment in reversed(zip(commit_ids, datestrings, authors, comments)):
        
	subprocess.check_output(["git", "reset", "--hard", commit_id])

	directory_translation = os.path.join(directory_experiment, "{}_{}".format(pilot, commit_id)) 
        filename_translation = os.path.join(directory_translation, "translation.txt")
        
        if not os.path.isdir(directory_translation): 
            #has not already been created in the past
            os.makedirs(directory_translation)
            #versions = git_pilotversion(filename_repository_translation)
            log.info("Moving translation to {}".format(filename_translation))
            shutil.copy(filename_repository_translation, filename_translation)
            description = get_custom_description(filename_repository_translation)
            create_system_description(directory_translation, pilot, datestring, commit_id, version, description, author, comment, url_repository_revisions)
            
        version+=1

    subprocess.check_output(["git", "pull", "origin", "master"])



if __name__ == '__main__':
    if sys.argv[1] == "-h":
        print "Usage: backend.py PATH_REPOSITORY_TRANSLATIONS PATH_FRONTEND_DATA_DIRECTORY URL_REPOSITORY_TRANSLATIOS [--debug]"
        sys.exit()
    
    loglevel = log.INFO
    loglevel = log.DEBUG
    log.basicConfig(level=loglevel,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')
    FORMAT = "%(asctime)-15s [%(process)d:%(thread)d] %(message)s "
    
    #path_code = sys.argv[1]
    path_repository = sys.argv[1] 
    path_evaluation = sys.argv[2] 
    url_repository_revisions = sys.argv[3]
    try:
        debug = (sys.argv[4]=="--debug")
    except:
        debug = False
    print debug
    update(path_repository, path_evaluation, url_repository_revisions, debug)
